package com.ikhokha.techcheck;

import java.util.regex.Pattern;

public class TextFileService {
	
	public int shorterComments(String line) {
		int count = 0;
        if (line.length() < 15 && !line.isEmpty()) {
        	count++;
        }
        return count;
	}
    
    public int findTotalWordOccurrence(String line, String comparer) {
		String[] words = line.split(" ");
		int count = 0;
		if (line.contains(comparer)) {
			for (String word : words) {
				count += word.contains(comparer) ? 1 : 0;
			}
		}
		
		return count;
	}
   
	public int findTotalUrlOccurrences(String line)
	{
		int count = 0;
		String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
		Pattern pattern = Pattern.compile(regex);
		java.util.regex.Matcher matcher = pattern.matcher(line);
		while(matcher.find()){
          count++;
		}

		return count;
	}

}