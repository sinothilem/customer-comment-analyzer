package com.ikhokha.techcheck;

import java.util.HashMap;
import java.util.Map;

public class StorageService {

	private final Map<String, Integer> totalResults = new HashMap<>();

	public synchronized void update(String key, Integer value) {

		totalResults.putIfAbsent(key, 0);
		totalResults.put(key, totalResults.get(key) + value);
	}

	public synchronized Map<String, Integer> currentData() {
		
		Map<String, Integer> currentData = new HashMap<>();
		totalResults.forEach((key,value)-> currentData.put(key, value));
		return currentData;
	}
}
