package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileProcessor implements Runnable {

	private final File file;
	private final StorageService storage;
	private TextFileService  textFileService;

	public FileProcessor(File file, StorageService storage) {
		this.file = file;
		this.storage = storage;
		textFileService = new TextFileService ();
	}

	@Override
	public void run() {
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {

			String line = null;
			while ((line = reader.readLine()) != null) {

				final String data = line.toLowerCase();
				
				addToStorage(textFileService.shorterComments(data), "SHORTER_THAN_15");
				addToStorage(textFileService.findTotalWordOccurrence(data, "mover"), "MOVER_MENTIONS");
				addToStorage(textFileService.findTotalWordOccurrence(data, "shaker"), "SHAKER_MENTIONS");
				addToStorage(textFileService.findTotalWordOccurrence(data, "?"), "QUESTIONS");
				addToStorage(textFileService.findTotalUrlOccurrences(data), "SPAM");
			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.getAbsolutePath());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("IO Error processing file: " + file.getAbsolutePath());
			e.printStackTrace();
		}

	}

	private void addToStorage(Integer occurrences, String key) {
		// TODO Auto-generated method stub
		if (occurrences > 0) {
			storage.update(key, occurrences);
		}
	}
}
