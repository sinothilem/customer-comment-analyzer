package com.ikhokha.techcheck;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {

	public static void main(String[] args) {
		
		File docPath = new File("docs");
		List<File> commentFiles = Arrays.asList(docPath.listFiles((d, n) -> n.endsWith(".txt")));
		
		StorageService storage = new StorageService();

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
        
        for (int i = 0; i < commentFiles.size(); i++) 
        {
        	FileProcessor task = new FileProcessor(commentFiles.get(i),storage);
            executor.execute(task);
        }
        executor.shutdown();
        
		try {	
		Thread.sleep(200);
	    } catch (InterruptedException e) {
		e.printStackTrace();
	   }
        
        if(executor.isTerminated()) {
        	System.out.println("RESULTS\n=======");
    		storage.currentData().forEach((k,v) -> System.out.println(k + " : " + v));
        }
	}

}
